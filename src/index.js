/* eslint-disable no-shadow, no-underscore-dangle */
import React from 'react'
import { render } from 'react-dom'
import buildDebug from 'debug'
// eslint-disable-next-line
import { ApolloClient, HttpLink, InMemoryCache, ApolloLink } from 'apollo-boost'
import merge from 'lodash.merge'
import { withClientState } from 'apollo-link-state'
// eslint-disable-next-line
import { concat, split } from 'apollo-link'
import { ApolloProvider } from 'react-apollo'
import { configure } from 'mobx'
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'mobx-react'
import { applySnapshot } from 'mobx-state-tree'
// load roboto font to entrypoint
// eslint-disable-next-line import/extensions
import 'typeface-roboto'
import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles'
import blue from 'material-ui/colors/blue'
import pink from 'material-ui/colors/pink'

import './index.css'
import { rootStore } from './stores'
import App from './App'
import ErrorBoundary from './ErrorBoundary'
import registerServiceWorker from './registerServiceWorker'

// eslint-disable-next-line
const debug = buildDebug('assembler-web:index')

const theme = createMuiTheme({
  palette: {
    primary: blue,
    secondary: pink,
  },
})

if (window._____SNAPSHOT_____) {
  applySnapshot(rootStore, JSON.parse(window._____SNAPSHOT_____))
}

const stores = {
  rootStore,
}

// For easier debugging
window._____APP_STATE_____ = stores

configure({ enforceActions: true })

const cache = new InMemoryCache()
const stateLink = withClientState({
  ...merge(),
  cache,
})
const httpLink = new HttpLink({
  uri: process.env.REACT_APP_HTTP_ENDPOINT,
  headers: process.env.REACT_APP_TOKEN && {
    authorization: `Bearer ${process.env.REACT_APP_TOKEN}`,
    'Accept-Language': 'en_US',
  },
})
const client = new ApolloClient({
  cache,
  link: ApolloLink.from([stateLink, httpLink]),
})

render(
  <ApolloProvider client={client}>
    <Provider {...stores}>
      <BrowserRouter>
        <MuiThemeProvider theme={theme}>
          <ErrorBoundary>
            <App />
          </ErrorBoundary>
        </MuiThemeProvider>
      </BrowserRouter>
    </Provider>
  </ApolloProvider>,
  document.getElementById('root'),
)

registerServiceWorker()
