/* eslint-disable no-underscore-dangle, no-console */
const debug = require('debug')('assembler-web:schema')
const { downloadSchema } = require('apollo-codegen')

// load env variables required for fetching the schema
require('dotenv').config({ path: '.env' })
require('dotenv').config({ path: '.env.local' })

debug(process.env.REACT_APP_HTTP_ENDPOINT)

async function start() {
  debug('start()')
  await downloadSchema(
    process.env.REACT_APP_HTTP_ENDPOINT, /* schema */
    './schema.json', /* output */
    {
      Authorization: `Bearer ${process.env.REACT_APP_TOKEN}`,
    }, /* header */
    false, /* insecure */
    'POST', /* method */
  )
}

start()
