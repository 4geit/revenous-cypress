/* eslint-disable react/no-multi-comp */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import AppBar from 'material-ui/AppBar'
import Toolbar from 'material-ui/Toolbar'
import TextField from 'material-ui/TextField'
import Typography from 'material-ui/Typography'
import Divider from 'material-ui/Divider'
import Icon from 'material-ui/Icon'

import { BusinessQuery } from '../graphql'

import BackgroundImage from './background_search_mobile.jpg'

@BusinessQuery
export default class DetailsComponent extends Component {
  static propTypes = {
    // eslint-disable-next-line
    business: PropTypes.object,
  }
  static defaultProps = {
    business: {},
  }

  render() {
    const { business } = this.props
    return (
      <div>
        <AppBar style={{ marginBottom: '50px' }} position="static" color="primary">
          <Toolbar>
            <Typography
              style={{ color: 'white' }}
              align="left"
              variant="display1"
            >
            Revenous
            </Typography>
            <div
              style={{
                position: 'absolute',
                right: '0px',
              }}
            >
              <TextField
                style={{
                  borderRadius: 4,
                  backgroundColor: 'white',
                  border: '1px solid #ced4da',
                  fontSize: '16px',
                  width: '100px',
                  marginRight: '10px',
                }}
                id="name-input"
                InputProps={{
                  disableUnderline: true,
                }}
              />
              <TextField
                style={{
                  borderRadius: 4,
                  backgroundColor: 'white',
                  border: '1px solid #ced4da',
                  fontSize: '16px',
                  width: '100px',
                  marginRight: '10px',
                }}
                id="location-input"
                InputProps={{
                  disableUnderline: true,
                }}
              />
            </div>
          </Toolbar>
        </AppBar>
        <div
          className="business-details"
          style={{
            margin: '0px 120px 30px 120px',
            alignItems: 'center',
            display: 'flex',
            jutifyContent: 'space-around',
            flexDirection: 'row',
          }}
        >
          <div
            className="name"
            style={{
              flex: '50%',
              flexDirection: 'column',
            }}
          >
            <Typography variant="display1">
              {business.name}
            </Typography>
            <Icon style={{ color: 'red' }}>stars_icon</Icon>
            <Icon style={{ color: 'red' }}>stars_icon</Icon>
            <Icon style={{ color: 'red' }}>stars_icon</Icon>
            <Icon style={{ color: 'lightGrey' }}>stars_icon</Icon>
            <Icon style={{ color: 'lightGrey' }}>stars_icon</Icon>
          </div>
          <div
            className="picture"
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              flex: '30%',
            }}
          >
            <img
              style={{ width: 150, height: 150 }}
              src={business.photos}
              alt=""
            />
          </div>
        </div>
        <div
          className="review-list"
          style={{ margin: '0px 120px 0px 120px' }}
        >
          <Typography variant="title" color="primary" gutterBottom>
            Recommended reviews
          </Typography>
        </div>
        <Divider />
        <div
          className="flex-container"
          style={{
            margin: '0px 120px 0px 120px',
            display: 'flex',
            justifyContent: 'space-around',
            flexDirection: 'column',
          }}
        >
          <div
            className="review"
            style={{
              display: 'flex',
              justifyContent: 'space-around',
              flexDirection: 'row',
              margin: '20px 0 0 0',
            }}
          >
            <div
              className="image"
              style={{
                display: 'flex',
                margin: '20px 50px 0px 0px',
                flexDirection: 'column',
              }}
            >
              <img
                style={{
                  height: '60px',
                  width: '60px',
                  borderRadius: '4px',
                }}
                src={BackgroundImage}
                alt=""
              />
              <Typography variant="body2">G.Baldins</Typography>
            </div>
            <div
              className="comment"
              style={{
                flex: '70%',
                display: 'flex',
              }}
            >
              <Typography>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                sed do eiusmod tempor incididunt ut labore et dolore magna
                aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                ullamco laboris nisi ut aliquip ex ea commodo consequat.
                Duis aute irure dolor in reprehenderit in voluptate velit
                esse cillum dolore eu fugiat nulla pariatur. Excepteur
                sint occaecat cupidatat non proident, sunt in culpa qui
                officia deserunt mollit anim id est laborum.
              </Typography>
            </div>
          </div>
          <Divider />
        </div>
      </div>
    )
  }
}
