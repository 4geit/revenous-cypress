/* eslint-disable react/no-multi-comp */
import React, { Component } from 'react'
import buildDebug from 'debug'
// eslint-disable-next-line
import PropTypes from 'prop-types'
import { inject, observer } from 'mobx-react'
// eslint-disable-next-line
import { Switch, Route, withRouter, Link } from 'react-router-dom'
import Input from 'material-ui/Input'
import Button from 'material-ui/Button'
import Icon from 'material-ui/Icon'
import Typography from 'material-ui/Typography'

import ResultComponent from './resultComponent'
import BackGroundImage from './background_search_desktop.jpg'

const debug = buildDebug('revenous-cypress:home-component')

@inject('rootStore')
@observer
export default class HomeComponent extends Component {
  static propTypes = {
    // eslint-disable-next-line
    rootStore: PropTypes.any.isRequired,
  }

  handleOnSubmit = (event) => {
    debug('handleOnSubmit()')
    const { rootStore } = this.props
    rootStore.submit()
    event.preventDefault()
  }
  handleOnWhereChange = ({ target: { value } }) => {
    debug('handleOnWhereChange()')
    const { rootStore } = this.props
    rootStore.setWhere(value)
  }
  handleOnSearchChange = ({ target: { value } }) => {
    debug('handleOnSearchChange()')
    const { rootStore } = this.props
    rootStore.setSearch(value)
  }

  render() {
    const { rootStore } = this.props
    const { submitted, where, search } = rootStore
    if (submitted) {
      if (!where) {
        return (
          <div id="error">
            You didnt define a location. Please set a location.
          </div>
        )
      }
      return (
        <ResultComponent search={search} where={where} />
      )
    }
    return (
      <div
        style={{
          backgroundImage: `url(${BackGroundImage})`,
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          backgroundSize: 'cover',
          backgroundRepeat: 'no-repeat',
          height: '25rem',
        }}
      >
        <div>
          <Typography
            style={{ color: 'white' }}
            variant="display3"
            align="center"
          >Revenous
          </Typography>
        </div>
        <form onSubmit={this.handleOnSubmit}>
          <div style={{ textAlign: 'center' }}>
            <Input
              placeholder="Type something here"
              style={{
                marginRight: '5px',
                marginBottom: '20px',
                color: 'white',
              }}
              value={search}
              onChange={this.handleOnSearchChange}
            />
            <Input
              placeholder="Location"
              style={{
                marginBottom: '20px',
                color: 'white',
              }}
              value={where}
              onChange={this.handleOnWhereChange}
            />
          </div>
          <div style={{ textAlign: 'center' }}>
            <Button
              style={{
                marginBottom: '50px',
              }}
              variant="raised"
              color="primary"
              type="submit"
            >
              Search
              <Icon style={{ marginLeft: 10 }}>search</Icon>
            </Button>
          </div>
        </form>
      </div>
    )
  }
}
