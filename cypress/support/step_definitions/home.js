/* global given, when, then */

then('I should see the title {string}', (title) => {
  cy.contains(title).should('be.visible')
})

then('I should see a text field with the placeholder {string}', (placeholder) => {
  cy.get(`input[placeholder="${placeholder}"]`).should('be.visible')
})
