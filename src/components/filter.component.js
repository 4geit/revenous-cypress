/* eslint-disable react/no-multi-comp */
import React, { Component } from 'react'
// eslint-disable-next-line
import PropTypes from 'prop-types'
// eslint-disable-next-line
import { Switch, Route, withRouter, Link } from 'react-router-dom'

import AppBar from 'material-ui/AppBar'
import Tabs, { Tab } from 'material-ui/Tabs'

export default class FilterComponent extends Component {
  state = {
    value: 0,
  }

  handleChange = (event, value) => {
    this.setState({ value })
  }

  render() {
    return (
      <div>
        <AppBar position="static" color="primary">
          <Tabs
            value={this.state.value}
            onChange={this.handleChange}
            centered
          >
            <Tab label="Best Match" />
            <Tab label="Highest Rated" />
            <Tab label="Most Reviewed" />
          </Tabs>
        </AppBar>
      </div>
    )
  }
}
