# Revenous-Cypress

Test app in order to setup cypress with a React app and a GraphQL client. The GraphQL requests are also mocked.

You can see a demo running @ http://staging.revenous-cypress.ws3.4ge.it/ and http://revenous-cypress.ws3.4ge.it/

## Installation

1. Clone the repository and get into the new folder by using the command:

```
git clone git@gitlab.com:4geit/revenous-cypress.git && cd revenous-cypress
```

2. You will need to first create a `.env.local` file that contains those variables:

```
REACT_APP_HTTP_ENDPOINT = https://cors-anywhere.herokuapp.com/https://api.yelp.com/v3/graphql
REACT_APP_TOKEN = YOUR_YELP_API_TOKEN
```

replace `YOUR_YELP_API_TOKEN` by the one yelp provides you under your developer yelp dashboard.

3. Then you can install the dependencies by running the command:

```
yarn
```

4. If you want to run the app locally, use the following command line:

```
yarn start
```

## Development/Testing

In order to run the integration tests, make sure you have the local app running by using the command `yarn start` and then you can open `cypress` by using the command:

```
yarn cypress open
```

It will open a new window where you can select whatever integration tests you want to test such as `search.feature`. Clicking on one will open a new browser window with all the tests related to this integration test running automatically and from there one you can start working in the code and replay the tests to make sure your changes did not introduce any failures in the tests.
