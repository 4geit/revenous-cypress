Feature: Search

  As a user I want to search for restaurants

  Scenario: landing
    Given I open the home page
    Then I should see the title "Revenous"
    Then I should see a text field with the placeholder "Type something here"
    Then I should see a text field with the placeholder "Location"

  Scenario: searching
    Given I open the home page
    When I search for "vegan" in the location "Paris"
    Then I should see a list of 4 items
    Then I should see a list of 4 names
    Then I should see a list of 4 images

  Scenario: searching without location
    Given I open the home page
    When I only search for "something" without a location
    Then I should see an error message "You didn't define a location. Please set a location."
    Then I should not see the results
