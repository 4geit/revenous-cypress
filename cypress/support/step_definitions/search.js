/* global given, when, then */

given('I open the home page', () => {
  cy.server()
  // FIXME: the image routing is not working
  // loading a fake item image used by search items
  cy.route('**/item/image/*', 'fixture:images/item.jpg')
  // visiting the homepage of the app
  cy.visitWithGraphQLStub('http://localhost:3000')
})

when('I search for {string} in the location {string}', (query, location) => {
  cy.get('input[placeholder="Location"]').type(location)
  cy.get('input[placeholder="Type something here"]').type(`${query}{enter}`)
})

when('I only search for {string} without a location', (query) => {
  cy.get('input[placeholder="Type something here"]').type(`${query}{enter}`)
})

then('I should see a list of {int} items', (total) => {
  cy.get('#results').children().should('have.length', total)
})

then('I should see a list of {int} names', (total) => {
  cy.get('#results .name').should('have.length', total)
})

then('I should see a list of {int} images', (total) => {
  cy.get('#results .image').should('have.length', total)
  cy.get('#results .image img').should('have.length', total)
})

then('I should see an error message {string}', (message) => {
  cy.get('#error').should('be.visible')
  cy.contains(message)
})

then('I should not see the results', () => {
  cy.get('#results').should('not.be.visible')
})

then('I see a list of results', () => {
  cy.search()
  cy.get('#results').should('be.visible')
})
