import buildDebug from 'debug'
import { graphql } from 'react-apollo'
import { gql } from 'apollo-boost'

const debug = buildDebug('revenous-cypress:search-query')

export const query = gql`
  query searchQuery(
    $search: String!
    $where: String!
  ) {
    search(
      term: $search
      location: $where
    ) {
      business {
        id
        __typename
        name
        photos
      }
      total
    }
  }`
export const config = {
  props: ({ data: { loading, error, search } }) => {
    debug('query:search()')
    if (loading) { return { loading } }
    if (error) { return { error } }
    const { business: businesses, total } = search
    return { loading: false, businesses, total }
  },
}
export default graphql(query, config)
