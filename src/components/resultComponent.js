/* eslint-disable react/no-multi-comp */
import React, { Component } from 'react'
import buildDebug from 'debug'
// eslint-disable-next-line
import PropTypes from 'prop-types'
// eslint-disable-next-line
import { Switch, Route, withRouter, Link } from 'react-router-dom'
import GridList, { GridListTile, GridListTileBar } from 'material-ui/GridList'
import Icon from 'material-ui/Icon'

import { SearchQuery } from '../graphql'
import FilterComponent from './filter.component'

const debug = buildDebug('revenous-cypress:result-component')

@withRouter
@SearchQuery
export default class ResultComponent extends Component {
  static propTypes = {
    loading: PropTypes.bool,
    // eslint-disable-next-line
    businesses: PropTypes.array,
    // eslint-disable-next-line
    history: PropTypes.object.isRequired,
  }
  static defaultProps = {
    loading: true,
    businesses: [],
  }

  handleOnClick = id => () => {
    debug('handleOnClick()')
    const { history } = this.props
    history.replace(`/${id}`)
  }

  render() {
    const { loading, businesses } = this.props
    if (loading) {
      return (
        <div id="loading">
          Currently loading…
        </div>
      )
    }
    return (
      <div style={{ margin: '0px 120px 0px 120px' }}>
        <FilterComponent />
        <GridList
          id="results"
          cols={1}
          cellHeight={180}
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            margin: 20,
          }}
        >
          { businesses.map(({ id, name, photos }) => (
            <GridListTile
              style={{
                alignItems: 'center',
                justifyContent: 'center',
              }}
              key={id}
              onClick={this.handleOnClick(id)}
            >
              <div className="image">
                { photos.map(photo => (
                  <img style={{ width: 250, height: 250 }} src={photo} alt="" />
                )) }
              </div>
              <GridListTileBar
                style={{
                  width: 250,
                  position: 'absolute',
                }}
                title={name}
                className="name"
                actionIcon={
                  <Icon
                    style={{
                      color: 'rgba(255, 255, 255, 0.54)',
                    }}
                  >
                    Info
                  </Icon>
                }
              />
            </GridListTile>
          )) }
        </GridList>
      </div>
    )
  }
}
