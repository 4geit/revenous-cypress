/* eslint-disable react/no-multi-comp */
import React, { Component } from 'react'
// eslint-disable-next-line
import PropTypes from 'prop-types'
// eslint-disable-next-line
import { Switch, Route, withRouter, Link } from 'react-router-dom'
import HomeComponent from './components/home.component'
import DetailsComponent from './components/details.component'

@withRouter
class App extends Component {
  static propTypes = {
    // TBD
  }

  render() {
    return (
      <div>
        <Switch>
          <Route path="/" exact component={HomeComponent} />
          <Route path="/:id" component={DetailsComponent} />
        </Switch>
      </div>
    )
  }
}

export default App
