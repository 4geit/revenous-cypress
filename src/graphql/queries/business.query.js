import buildDebug from 'debug'
import { graphql } from 'react-apollo'
import { gql } from 'apollo-boost'

const debug = buildDebug('revenous-cypress:business-query')

export const query = gql`
  query businessQuery(
    $id:String!
  ) {
    business(id: $id) {
      name
      photos
    }
  }`
export const config = {
  props: ({ data: { loading, error, business } }) => {
    debug('query:business()')
    if (loading) { return { loading } }
    if (error) { return { error } }
    const { id } = business
    return { loading: false, id }
  },
}
export default graphql(query, config)
