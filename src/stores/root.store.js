/* eslint-disable no-param-reassign */
import { types } from 'mobx-state-tree'

export const RootStore = types
  .model({
    submitted: types.optional(types.boolean, false),
    where: types.optional(types.string, ''),
    search: types.optional(types.string, ''),
  })
  .actions(self => ({
    setWhere(where) {
      self.where = where
    },
    setSearch(search) {
      self.search = search
    },
    submit() {
      self.submitted = true
    },
    reset() {
      self.where = ''
      self.search = ''
      self.submitted = false
    },
  }))

export default RootStore.create()
