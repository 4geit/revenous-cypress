/* eslint-disable no-underscore-dangle, no-param-reassign */
// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

const responseStub = result => Promise.resolve({
  json: () => Promise.resolve(result),
  text: () => Promise.resolve(JSON.stringify(result)),
  ok: true,
})

Cypress.Commands.add('visitWithGraphQLStub', (url, options) => {
  cy.fixture('graphql').then((mockedData) => {
    function fetch(path, { body /* , method */ }) {
      const { operationName } = JSON.parse(body)
      return responseStub(mockedData[operationName])
    }
    cy.visit(url, Object.assign({
      onBeforeLoad(win) {
        cy.stub(win, 'fetch')
          .withArgs('https://cors-anywhere.herokuapp.com/https://api.yelp.com/v3/graphql')
          .as('fetchGraphQL')
          .callsFake(fetch)
      },
    }, options))
  })
})

Cypress.Commands.add('search', () => {
  cy.on('window:before:load', (win) => {
    win._____SNAPSHOT_____ = JSON.stringify({
      location: 'Paris',
      search: 'Vegan',
      submitted: true,
    })
  })
  cy.visitWithGraphQLStub('http://localhost:3000')
})
